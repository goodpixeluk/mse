<?php

/**
 * Load a separate functions file.
 * @param  string $filename The filename to load.
 */
function _load_child_include( $filename ) {
  require_once 'includes/' . $filename . '.php';
}

/**
 * Load included files.
 */
_load_child_include( 'enqueue-assets' );
_load_child_include( 'image-sizes' );
_load_child_include( 'register-shortcodes' );
_load_child_include( 'acf-fc-titles' );
// _load_child_include( 'contact-form-handler' );

// add_filter('mce_buttons_2', 'bc_add_super_subscript_mce_buttons');
// function bc_add_super_subscript_mce_buttons( $buttons ) {  
//     array_unshift($buttons, 'superscript', 'subscript');

//     return $buttons;
// }

// add_filter('acf/fields/wysiwyg/toolbars', 'bc_add_super_subscript_acf_mce_buttons');
// function bc_add_super_subscript_acf_mce_buttons( $buttons ) {  
//     array_unshift($buttons['Full'][2], 'superscript', 'subscript');

//     return $buttons;
// }

/**
 * Setup the theme.
 */
add_action( 'after_setup_theme', 'bc_setup_theme' );
function bc_setup_theme() {

	add_theme_support('post-thumbnails');
	load_theme_textdomain( 'netzerosolihull' );

	/**
	 * Register menus
	 */
	register_nav_menus( array(
    'header-menu' => __( 'Header Menu' ),
    'footer-menu' => __( 'Footer Menu' ),
    'policies'    => __( 'Policies' ),
  ) );

	/**
	 * Add custom image sizes
	 */
	foreach ( bc_get_theme_image_sizes() as $crop_name => $crop_values) {
		add_image_size( $crop_name, $crop_values['width'], $crop_values['height'], $crop_values['hard_crop'] );
	}


	if ( class_exists('ACF') ) {
		add_action( 'admin_head', 'bc_admin_acf_styles' );
		add_action( 'admin_footer', 'bc_admin_acf_scripts', 999 );
	}
	
	/**
	 * Remove query strings from loaded resources
	 */
	add_action( 'script_loader_src', 'bc_remove_script_version' );
	add_action( 'style_loader_src', 'bc_remove_script_version' );

	
	/**
	 * Load our theme resources
	 */
	add_filter( 'get_asset_uri', 'bc_revision_assets', 20, 2 );
	add_action( 'wp_enqueue_scripts', 'bc_enqueue_styles' );
	add_action( 'wp_enqueue_scripts', 'bc_enqueue_scripts' );
	add_action( 'wp_enqueue_scripts', 'bc_enqueue_google_scripts' );
	add_filter('page_has_google_map', 'bc_page_has_google_map_layout');
	
	/**
	 * Clean up header tags
	 * @see https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/ 
	 */
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_shortlink_wp_head');
	remove_action('wp_head', 'rest_output_link_wp_head', 10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
	remove_action('template_redirect', 'rest_output_link_header', 11, 0);
}

/**
 * Remove query string from enqueued assets.
 * Opinionated, as this can cause assets to be cached more often.
 * Plugin or WP core assets will be especially susceptible to this, as they will
 * normally use this query string to force the new version.
 * A good cache invalidation strategy will be needed to ensure this works as intended.
 * @param  string $src The URI of an enqueued asset.
 * @return string      
 */
function bc_remove_script_version( $src ) {
	if ( !is_admin() ) {
		$src = remove_query_arg( 'ver', $src );
	}
	
	return $src;
}

/**
 * Replace any revisioned assets with their new revision names.
 * @param  string $uri  The full URI to the asset.
 * @param  string $path The original path relative to the theme directory.
 * @return string
 */
function bc_revision_assets( $uri, $path ) {
	$manifest_filepath = trailingslashit( get_stylesheet_directory() ) . 'dist/rev-manifest.json';

	if ( file_exists( $manifest_filepath ) && is_readable( $manifest_filepath ) ) {
		$manifest = json_decode( file_get_contents( $manifest_filepath ), true );

		foreach ($manifest as $original_name => $revisioned_name) {
			if ( preg_match( "#" . $original_name . "$#", $uri ) ) {
				$uri = str_replace( $original_name, $revisioned_name, $uri );
				break;
			}
		}
	}

	return $uri;
}

function bc_admin_acf_styles() {
	?>
<style>
.acf-flexible-content .layout.-modal {
    max-width: 1080px;
}

ul.acf-swatch-list label.selected:after {
    content: " (selected)";
}

.layout[data-layout="section"]~.layout:not(.-modal) {
    margin-left: 50px;
}

.layout.layout:not(.-modal)[data-layout="section"] {
    margin-left: 0;
}

.acf-fc-layout-preview {
    display: block;
    max-width: 90%;
    margin-left: 26px;
}

.select2-swp-acf-si__icon {
    stroke: currentcolor;
    /* Will only be applied if the fill is inherit */
    fill: currentcolor;
}

.select2-swp-acf-si__name {
    margin-left: 1em;
}

.acf-postbox.seamless>.postbox-header {
    display: none;
}

#postimagediv .inside img[src$=".svg"] {
    width: 100%;
}
</style>
<?php
}

function bc_admin_acf_scripts() {
	?>
<script>
// Enhance ACF Swatch plugin with selected text
var $swatches = jQuery('ul.acf-swatch-list input[type=radio]');

$swatches.on('change', function() {
    jQuery(this).closest('ul.acf-swatch-list').find('label.selected').removeClass('selected');
    jQuery(this).closest('label').addClass('selected');
});
</script>
<?php
}

/**
 * Get an adjacent block in the current have_rows() loop.
 * @param  integer $direction -1, < 0 	  == previous block,
 *                            0, +1, >= 0 == next block.
 * @return array
 */
function bc_acf_get_adjacent_layout( $direction = 1 ) {
	global $post;

	$loop = acf_get_loop('active');
	$direction = $direction >= 0 ? 1 : -1;

	$adjacent_index = $loop['i'] + $direction;

	if ( !isset($loop['value'][$adjacent_index]) ) {
		return false;
	}

	$value = acf_format_value( $loop['value'], $loop['post_id'], $loop['field'] );
	$block = acf_maybe_get( $value, $adjacent_index );

	return $block;
}

/**
 * Check if a post has at least 1 of a particular layout.
 * @param  string 		   $layout_name The name of a layout.
 * @param  WP_Post|integer $_post       Optional, A post object or post ID.
 * @return boolean
 */
function bc_acf_post_has_layout( $layout_name, $_post = null ) {
	$_post = get_post($_post);

	$layouts = get_field('layout', $_post);
	
	if ( !$layouts ) {
		return false;
	}

	foreach ($layouts as $key => $layout) {
		if ( $layout_name === $layout['acf_fc_layout'] ) {
			return true;
		}
	}

	// If got here, there are none of this layout
	return false;
}

function bc_page_has_google_map_layout( $has_map ) {
	global $post;

	return bc_acf_post_has_layout('google_map', $post);
}

function bc_get_blog_queries() {
	// CONSTANTS
	$POSTS_PER_PAGE = 5;
	$FEATURED_POSTS_PER_PAGE = 1;

	$paged = (int)get_query_var('paged');
	if ( $paged < 1 ) {
		$paged = 1;
	}

	$featured_query_args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => $FEATURED_POSTS_PER_PAGE,
		'meta_query' => array(
			array(
				'key' => 'is_featured',
				'value' => 1,
				'compare' => '=',
				'type' => 'NUMERIC',
			),
		),
		'order' => 'DESC',
		'orderby' => 'date',
	);
	$featured_query = new WP_Query( $featured_query_args );

	if ( !$featured_query->have_posts() ) {
		unset($featured_query_args['meta_query']);
		$featured_query = new WP_Query( $featured_query_args );
	}

	// calc custom offset
	$offset = 0;
	if ( $paged > 1 ) {
		$offset = ($POSTS_PER_PAGE - $FEATURED_POSTS_PER_PAGE) + (($paged - 2) * $POSTS_PER_PAGE);
	}

	$main_query_args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => $paged > 1 ? $POSTS_PER_PAGE : $POSTS_PER_PAGE - $FEATURED_POSTS_PER_PAGE,
		'offset' => $offset,
		'paged' => $paged,
		'post__not_in' => array($featured_query->posts[0]->ID),
		'order' => 'DESC',
		'orderby' => 'date',
	);
	$main_query = new WP_Query( $main_query_args );

	$all_query_args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => $POSTS_PER_PAGE,
		'paged' => $paged,
		'order' => 'DESC',
		'orderby' => 'date',
	);
	$all_query = new WP_Query( $all_query_args );

	return array(
		'featured' => $featured_query,
		'main' => $main_query,
		'all' => $all_query,
		'paged' => $paged,
	);
}

/**
 * Get the referer for a blog post, based on a set of patterns.
 * The URL returned will always be related to the posts listing in some way.
 * @return string 
 */
function bc_get_post_referer() {
	$referer = wp_get_referer();

	$safe_referer = home_url('/blog/');
	$safe_patterns = array(
		'#/blog/#',
		'#/blog/page/\d+/#'
	);

	foreach ($safe_patterns as $pattern) {
		if ( preg_match($pattern, $referer) ) {
			$safe_referer = $referer;
			break;
		}
	}

	return $safe_referer;
}

function bc_get_spacing_css_style( $spacing ) {
	$above = $spacing['above'];
	$below = $spacing['below'];

	if ( !$above && !$below ) {
		return '';
	}

	// above must be padding to prevent margin collapse
	$above_style = '';
	if ( 0 != $above ) {
		$above_style = sprintf('margin-top: %dvw;', $above);
	}

	$below_style = '';
	if ( 0 != $below ) {
		$below_style = sprintf('margin-bottom: %dvw;', $below);
	}

	return sprintf(
		'style="%s %s"',
		$above_style,
		$below_style
	);
}

/**
 * Check if the file URI passed represents a downloadable file.
 * @param  string $uri A file URI, e.g. images/image.jpg, documents/file.pdf
 * @return boolean
 */
function bc_is_download_uri( $uri ) {
	// These extensions should NOT be classed as a download
	// They are more likely to represent a web page
	$excluded_extensions = array(
		'asp',
		'aspx',
		'cer',
		'cfm',
		'cgi',
		'css',
		'htm',
		'html',
		'js',
		'jsp',
		'part',
		'php',
		'pl',
		'py',
		'rss',
		'xhtml',
		'xml',
	);

	$path = parse_url( $uri, PHP_URL_PATH );
	$ext = pathinfo( $path, PATHINFO_EXTENSION );

	return $ext && !in_array( $ext, $excluded_extensions );
}

if ( function_exists('register_sidebar') ) {
    $sidebar1 = array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',        
        'name'=>__( 'About Us', 'mseuk' ),
		'id' => 'about-us',
    );  
    $sidebar2 = array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',        
        'name'=>__( 'Services', 'mseuk' ),
		'id'	=> 'services',
    );
    $sidebar3 = array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',        
        'name'=>__( 'News', 'mseuk' ),
		'id'	=> 'news',
    );

	$sidebar4 = array(
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',        
        'name'=>__( 'Contact', 'mseuk' ),
		'id'	=> 'contact',
    );

    register_sidebar($sidebar1);
    register_sidebar($sidebar2);
    register_sidebar($sidebar3);
	register_sidebar($sidebar4);
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer Settings',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	
}
 
class Testimonial_Widget extends WP_Widget {
 
    function __construct() {
 
        parent::__construct(
            'testimonials',  // Base ID
            'Testimonials'   // Name
        );
 
        add_action( 'widgets_init', function() {
            register_widget( 'Testimonial_Widget' );
        });
 
    }
 
    public $args = array(
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
    );
 
    public function widget( $args, $instance ) {
		
		echo '<div class="slider slider--testimonials">';

		$testimonials = get_field('testimonials', 'options');
		
        echo $args['before_widget'];
 
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
		
        echo '<div class="js-slider">';
		
		if ( $testimonials ) {
        	foreach( $testimonials as $item ) {
				echo '<div class="testimonial-slide">';
				echo '<h4>' . $item['title'] . '</h4>';
				echo '<p>' . $item['quote'] . '</p>';
				echo '<p class="author">' . $item['author'] . '<br>';
				echo '<span class="author-position">' . $item['position'] . '</p>';
				echo '</div>';
			}
		}
		echo '</div>';
		echo '<div class="slider__buttons">';
		echo '<button class="prev"><span class="sr-only">Previous Slide</span></button>';
		echo '<button class="next"><span class="sr-only">Next Slide</span></button>';
		echo '</div>';
 
        echo $args['after_widget'];

		echo '</div>';
 
    }
 
    public function form( $instance ) {
 
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'text_domain' );
   
        ?>
<p>
    <label
        for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php echo esc_html__( 'Title:', 'text_domain' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
        name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text"
        value="<?php echo esc_attr( $title ); ?>">
</p>
<?php
 
    }
 
    public function update( $new_instance, $old_instance ) {
 
        $instance = array();
 
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
 
        return $instance;
    }
 
}
$my_widget = new Testimonial_Widget();
?>