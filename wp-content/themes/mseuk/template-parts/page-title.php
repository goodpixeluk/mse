<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php if ( is_archive() ) { ?>
                <h1>Monthly Archives: <?php the_date('F Y'); ?></h1>
                <?php } else { ?>
                <h1><?php the_title(); ?></h1>
                <?php } ?>
            </div>

            <div class="col-md-6 breadcrumbs">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
            </div>

        </div>
    </div>
</div>