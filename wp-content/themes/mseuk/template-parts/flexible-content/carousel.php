<?php

$slides = get_sub_field('slides');
$title = get_sub_field('title');
$title_colour = get_sub_field('title_colour');
$spacing = get_sub_field('spacing');

if ( $slides ) { $numrows = count($slides); }

if( have_rows('slides') ): ?>

<div class="offset-lg-2 pt-<?php echo $spacing['above']; ?> pb-<?php echo $spacing['below']; ?>">

	<?php if ( $title ): ?>

	<h2 data-aos="fade-up" class="mb-8 offset-md-2 offset-lg-0 carousel-title" style="color: <?= $title_colour; ?>;">
			<?= $title; ?>	
	</h2>

	<?php endif; ?>

	<div class="glide overlay-carousel">

		<div class="glide__overlay">
      		<div class="glide__text">

      			<?php $count = 0; while( have_rows('slides') ) : the_row(); $count++;

	        		$classes = 'text__slide';

	        		$bg = get_sub_field('background_colour');
	        		$tc = get_sub_field('text_colour');
	        		$text = get_sub_field('text');

	        		if ( $count == 1 ) {
	        			$classes .= ' active';
	        		} ?>

	        		<div class="<?php echo $classes; ?>" data-slide="<?php echo $count; ?>" style="background-color: <?= $bg; ?>; color: <?= $tc; ?>">
		      			<?= $text; ?>
		      		</div>

	        	<?php endwhile; ?>

    			<!-- <span class="glide__current"><span class="current">1</span> / <?php // echo $numrows; ?></span> -->
      		</div>

		    <div class="glide__arrows" data-glide-el="controls">
				<button class="glide__arrow glide__arrow--left" data-glide-dir="<" disabled>
					<?php include_asset('static/svg/chevron-left.svg'); ?>
				</button>
				<button class="glide__arrow glide__arrow--right" data-glide-dir=">">
					<?php include_asset('static/svg/chevron-right.svg'); ?>
				</button>
			</div>
		</div>

		
      <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">

        	<?php while( have_rows('slides') ) : the_row(); 

        		$image = get_sub_field('image'); ?>

        		<li class="glide__slide">
		          	<?php echo wp_get_attachment_image( $image['id'], 'carousel', false, '' ); ?>
		        </li>

        	<?php endwhile; ?>
         
        </ul>
      </div>

      <div class="glide__bullets" data-glide-el="controls[nav]">

				<?php $count = 0; while( have_rows('slides') ) : the_row(); ?>
				    <button class="glide__bullet" data-glide-dir="=<?= $count; ?>"></button>
				<?php $count++; endwhile; ?>
			</div>


    </div><!-- /.glide -->
</div><!-- /.container-fluid -->

<?php endif; ?>