<?php
/* Content + Image Block Template */

$content = get_sub_field('content');
$title = get_sub_field('title');
$image = get_sub_field('image');
$position = get_sub_field('image_position');
$half_columns = get_sub_field('half_columns');

$class_a = 'col-lg-4';
$class_b = 'col-lg-8';

if ( $half_columns ) {
    $class_a = 'col-lg-6';
    $class_b = 'col-lg-6';
}

?>

<div class="row">
    <div class="<?= $class_a; ?> text-center mb-5 mb-lg-0 <?php if( $position == 'right') { ?> order-lg-2<?php } ?>">
        <?= wp_get_attachment_image($image['id'], 'full'); ?>
    </div>

    <div class="<?= $class_b; ?> <?php if( $position == 'right') { ?> order-lg-1<?php } ?>">
        <div class="stack">
            <?php if ($title) { ?><h3 class="line-title"><?= $title; ?></h3><?php } ?>
            <?= $content; ?>
        </div>
    </div>
</div>