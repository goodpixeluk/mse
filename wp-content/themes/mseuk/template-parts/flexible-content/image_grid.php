<?php
/* Buttons Template */

$images = get_sub_field('images');
?>

<div class="row">
    <?php foreach($images as $image): ?>
    <div class="col-6 col-md mb-5 mb-md-0">
        <?= wp_get_attachment_image($image['id'], 'full'); ?>
    </div>
    <?php endforeach; ?>
</div>