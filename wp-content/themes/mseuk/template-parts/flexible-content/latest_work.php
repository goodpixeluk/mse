<?php
/* Content + Form Block Template */

$description = get_sub_field('description');
$title = get_sub_field('title');

$args = array(
    'post_type'         => 'project',
    'post_status'       => 'publish',
    'posts_per_page'    => 3,
);

$projects = new WP_Query( $args );

?>

<div class="row">
    <div class="col-lg-3 col-md-6 stack mb-lg-0 mb-4">
        <h3 class="line-title"><?= $title; ?></h3>
        <p><?= $description; ?></p>
    </div>

    <?php if ( $projects->have_posts() ):
                
    while ( $projects->have_posts() ): $projects->the_post(); 
    
    $pt_id = get_post_thumbnail_id(); ?>

    <div class="col-lg-3 col-md-6 mb-lg-0 mb-4">
        <div class="flip-card">
            <a href="<?php the_permalink(); ?>" class="flip-card__img flip-card__img--bg flip-card__img--home"
                style="background-image: url('<?php echo wp_get_attachment_image_url($pt_id, 'flip_card'); ?>')">
            </a>
            <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
        </div>
    </div>

    <?php endwhile;

    endif; wp_reset_postdata(); ?>
</div>