<?php
/* Image Boxes Template */

?>

<div class="row">


    <?php if( have_rows('image_boxes') ): while( have_rows('image_boxes') ) : the_row(); 
        
        $image = get_sub_field('image');
        $title = get_sub_field('title');
        $text = get_sub_field('text');
        $button = get_sub_field('button');

        if ( $button ):
            $button_url = $button['url'];
            $button_title = $button['title'];
            $button_target = $button['target'] ? $button['target'] : '_self';
        endif;
        
        ?>

    <div class="col text-center">
        <div class="image-box">
            <div class="image-box__image">
                <?= wp_get_attachment_image($image['id'], 'image_box' ); ?>
            </div>
            <div class="image-box__content">
                <h3><?= $title; ?></h3>
                <?= $text; ?>
                <?php if ( $button ): ?>
                <a class="button button--small" href="<?= $button_url; ?>" target="<?= $button_target; ?>">
                    <?= $button_title; ?>
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php endwhile; endif; ?>
</div>