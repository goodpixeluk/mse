<?php 

/* Image Block */

$image = get_sub_field('image');

?>


<div class="row">
    <div class="col-lg-12">
        <?= wp_get_attachment_image($image['id'], 'full'); ?>
    </div>
</div>