<?php

$icon_boxes = get_sub_field('icon_boxes');
$position = get_sub_field('icon_position');

$num_rows = count($icon_boxes);

if ( $num_rows == 1 ) {
	$col = 'col';
} elseif ( $num_rows == 2 ) {
	$col = 'col-md-6';
} elseif ( $num_rows == 3 ) {
	$col = 'col-md-4';
} elseif ( $num_rows == 4 ) {
	$col = 'col-lg-3 col-md-6';
} else {
	return;
}

?>

<?php if( have_rows('icon_boxes') ): ?>


<div class="row">

    <?php while( have_rows('icon_boxes') ) : the_row(); $count++;

		    	$icon = get_sub_field('imageicon');
                $image_link = get_sub_field('image_link');
		    	$title = get_sub_field('title');
		    	$text = get_sub_field('text');
				$button = get_sub_field('button');

		    	if ( $button ):
		    		$button_url = $button['url'];
				    $button_title = $button['title'];
				    $button_target = $button['target'] ? $button['target'] : '_self';
		    	endif;

		    ?>

    <div class="<?= $col; ?>">
        <div class="icon_box icon_box--<?= $position; ?>">
            <div class="icon_box__image">
                <?php if ( $image_link ) { ?>
                <a href="<?= $image_link; ?>" target="_blank" rel="noopener">
                    <?php } ?>
                    <?= wp_get_attachment_image($icon['id'], 'full' ); ?>
                    <?php if ( $image_link ) { ?>
                </a>
                <?php } ?>
            </div>
            <div class="icon_box__content">
                <h4><?= $title; ?></h4>
                <p><?= $text; ?></p>
                <?php if ( $button ): ?>
                <a class="button" href="<?= $button_url; ?>" target="<?= $button_target; ?>">
                    <?= $button_title; ?>
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php endwhile; ?>

</div>


<?php endif; ?>