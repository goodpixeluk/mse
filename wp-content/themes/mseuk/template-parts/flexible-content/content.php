<?php
/* Content Block Template */

$content = get_sub_field('content');
$background = get_sub_field('background');

?>

<div class="row">
    <div class="col-lg-12">
        <div class="stack <?php if($background) { ?>grey-box<?php } ?>">
            <?= $content; ?>
        </div>
    </div>
</div>