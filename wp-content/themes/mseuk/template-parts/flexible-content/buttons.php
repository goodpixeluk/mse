<?php
/* Buttons Template */

$buttons = get_sub_field('buttons');
?>

<div class="row">
    <div class="col-lg-12">
        <div class="button-group">
            <?php foreach($buttons as $button): 
            
            $link = $button['button'];
            
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <a class="button" href="<?php echo esc_url( $link_url ); ?>"
                target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>


            <?php endforeach; ?>
        </div>
    </div>
</div>