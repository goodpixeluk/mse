<?php
/* Content + Form Block Template */

$content = get_sub_field('content');
$title = get_sub_field('title');
$form_title = get_sub_field('form_title');
$contact = get_sub_field('contact_form_shortcode');
$half_columns = get_sub_field('half_columns');

$class_a = 'col-lg-4';
$class_b = 'col-lg-8';

if ( $half_columns ) {
    $class_a = 'col-lg-6';
    $class_b = 'col-lg-6';
}

?>

<div class="row">
    <div class="<?= $class_b; ?>">
        <div class="stack">
            <?php if ($title) { ?><h3 class="line-title"><?= $title; ?></h3><?php } ?>
            <?php if ($content) { echo $content; } ?>
        </div>
    </div>

    <div class="<?= $class_a; ?> mt-lg-0 mt-5">
        <div class="stack">
            <?php if ($form_title) { ?><h3 class="line-title"><?= $form_title; ?></h3><?php } ?>
            <?php if ($contact) { echo do_shortcode($contact); } ?>
        </div>
    </div>
</div>