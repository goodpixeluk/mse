<?php
/* CF Template */

$title = get_sub_field('title');
$form = get_sub_field('contact_form_shortcode');

?>

<div class="row">
    <div class="col-lg-12 stack">
        <h3 class="line-title"><?= $title; ?></h3>
        <?= do_shortcode($form); ?>
    </div>
</div>