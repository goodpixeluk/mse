<?php
/* Image Boxes Template */

?>

<div class="row">


    <?php if( have_rows('service_boxes') ): while( have_rows('service_boxes') ) : the_row(); 
        
        $title = get_sub_field('title');
        $text = get_sub_field('text');

    ?>

    <div class="col-md-6 mb-5">
        <div class="service_box">
            <div class="service_box__content stack">
                <h3><?= $title; ?></h3>
                <?= $text; ?>
            </div>
        </div>
    </div>

    <?php endwhile; endif; ?>
</div>