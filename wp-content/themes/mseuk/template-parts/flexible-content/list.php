<?php
/* List Block Template */

$two_columns = get_sub_field('span_two_columns');

?>

<div class="row">
    <div class="col-lg-12">
        <div class="stack">
            <ul class="list <?php if ( $two_columns ) { ?>list--columns<?php } ?>">
                <?php if( have_rows('list') ): while( have_rows('list') ) : the_row(); 
                
                $link = get_sub_field('link');

                ?>

                <?php if ( $link ):
                    $new_tab = get_sub_field('open_in_new_tab');
                    $link_target = $new_tab ? '_blank' : '_self';    
                ?>

                <li>
                    <a href="<?= $link; ?>" target="<?= $link_target; ?>">
                        <?php the_sub_field('list_item'); ?>
                    </a>
                </li>

                <?php else: ?>

                <li><?php the_sub_field('list_item'); ?></li>

                <?php endif; ?>

                <?php endwhile; endif; ?>
            </ul>
        </div>
    </div>
</div>