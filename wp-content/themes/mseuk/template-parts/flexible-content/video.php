<?php
/* Text Block Template */

$video = get_sub_field('video');
$spacing = get_sub_field('spacing');
$caption = get_sub_field('caption');
?>
<div class="container-fluid pt-<?php echo $spacing['above']; ?> pb-<?php echo $spacing['below']; ?>">
	<div class="row">
		<div class="col-md-20 offset-md-2">
			<div class="embed-container">
				<?php echo $video; ?>
			</div>
			<?php if ( $caption ) : ?>
				<span class="caption"><?= $caption; ?></span>
			<?php endif; ?>
		</div>
	</div>
</div>