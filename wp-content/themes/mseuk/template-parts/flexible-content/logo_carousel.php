<?php 

/* Logo Carousel Block */

$logos = get_field('client_logos', 'options');

?>

<div class="row">
    <div class="col-lg-12">
        <div class="slider slider--clients">
            <h3 class="line-title mb-5">Our Clients</h3>
            <div class="js-slider">
                <?php foreach( $logos as $item ): ?>
                <div class="logo-slide">
                    <?= wp_get_attachment_image($item['id'], 'full'); ?>
                </div>
                <?php endforeach; ?>
            </div>

            <div class="slider__buttons slider__buttons--alt">
                <button class="prev"><span class="sr-only">Previous Slide</span></button>
                <button class="next"><span class="sr-only">Next Slide</span></button>
            </div>
        </div>


    </div>
</div>