<?php

$args = array(
	'post_type'			=>	'post',
	'posts_per_page'	=>	6,
);

$post_query = new WP_Query($args);
$title = get_sub_field('title');

?>

<?php if ( $post_query->have_posts() ): ?>

<div class="row">
    <div class="col-lg-12">
        <div class="slider slider--news">
            <h3 class="line-title mb-5"><?php echo $title; ?></h3>
            <div class="js-slider">
                <?php while ( $post_query->have_posts() ): $post_query->the_post(); ?>
                <div class="post-preview stack">
                    <?php $thumbnail = get_post_thumbnail_id(); ?>

                    <?php if ( $thumbnail ) { ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php echo wp_get_attachment_image( $thumbnail, 'news_thumb' ); ?>
                    </a>
                    <?php } ?>

                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                    <a class="link" href="<?php the_permalink(); ?>">Read more</a>
                </div>
                <?php endwhile; ?>
            </div>

            <div class="slider__buttons">
                <button class="prev"><span class="sr-only">Previous Slide</span></button>
                <button class="next"><span class="sr-only">Next Slide</span></button>
            </div>
        </div>


    </div>
</div>

<?php else: ?>

<?php endif; wp_reset_postdata(); ?>