<?php

/**
 * Flexible content partial: Start Section with Background Colour
 */

$theme = 'light';
$width = get_sub_field('section_width');

if ( get_sub_field('text_colour') ) {
	$theme = 'dark';
}

global $_section_open;

$colour = get_sub_field('colour');

?>

<?php if ( $_section_open ): ?>

<div class="section <?= $theme . ' ' . 'section--' .$width ?>" <?php if ( 'transparent' !== $colour ): ?> <?php
			// $colour_props = bc_get_hex_props($colour);

			// $text_color = '';
			// if ( $colour_props['lightness'] < LIGHTNESS_THRESHOLD ) {
			// 	$text_color = 'white';
			// }
		?> style="background-color:<?php echo $colour ?>;" <?php else: ?> style="" <?php endif; ?>>
    <div class="container">

        <?php $_section_open = false; ?>

        <?php elseif ( !$_section_open ): ?>

    </div><!-- /.container -->
</div> <!-- /.section -->
<?php $_section_open = true; ?>

<?php endif; ?>