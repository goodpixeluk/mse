<?php

?>

<div class="hero">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-lg-0 mb-5">
                <div class="hero__content">
                    <h1>Mechanical &amp; Electrical Contractors</h1>
                    <ul class="hero__list">
                        <li><a href="#">Electrical</a></li>
                        <li><a href="#">Air-conditioning</a></li>
                        <li><a href="#">Ventilation & Heating</a></li>
                        <li><a href="#">Data Networks</a></li>
                        <li><a href="#">Building Maintenance</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/static/images/hero-images.png"
                    alt="Mechanical & Electrical Contractors.">
            </div>
        </div>
    </div>
</div>