<section id="newsletter" class="section" style="background-color: #ededed;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="line-title">Subscribe to our newsletter</h3>
                <?php echo do_shortcode('[contact-form-7 id="8124" title="Newsletter"]'); ?>
            </div>
        </div>
    </div><!-- /.container -->
</section>