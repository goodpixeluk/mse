<?php

/**
 * Partial to show the flexible content (blocks) layout
 */
	
?>

<?php while ( have_rows('layout') ): the_row(); ?>
<?php
		$layout = get_row_layout();
		$prev_layout = bc_acf_get_adjacent_layout(-1);
		$next_layout = bc_acf_get_adjacent_layout(1);

		get_template_part('template-parts/flexible-content/'.$layout);
	?>
<?php endwhile; ?>