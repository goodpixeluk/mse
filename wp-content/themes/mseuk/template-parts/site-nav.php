<?php
	
/**
 * Template part for the main navigation menu.
 */

try {
	$menu = new WP_Menu_Query( array(
		'location' => 'header-menu',
	) );
} catch (Exception $e) {
	return;	
}

if ( !$menu->have_items() ) {
	return;
}

$menu_length = $menu->item_count;
$half_menu = floor($menu_length / 2);
$count = 0;


?>

<nav id="js-site-nav" class="site-nav" aria-labelledby="site-nav-label">

    <ul class="site-nav__list">

        <?php while ( $menu->have_items() ): $count++; ?>

        <?php
				$item = $menu->the_item();
				$classes = $item->classes;

				if ( $item->is_current() || $item->has_current_child() ) {
					$classes[] = 'is-current';
				}

				if ( $item->has_children() ) {
					$classes[] = 'has-children';
				}
			?>

        <li class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">

            <a class="site-nav__item" href="<?php echo esc_url( $item->url ); ?>"
                <?php echo ( $item->target ? 'target="' . $item->target . '"' : '' ); ?>>
                <span><?php echo $item->title; ?></span>
            </a>

        </li>

        <?php endwhile; ?>

    </ul>

</nav>