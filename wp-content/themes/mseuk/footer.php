<?php

/**
 * The template for displaying the footer.
 */

$client_logos = get_field('client_logos');
$contact_form = get_field('contact_form');
$cf_shortcode = get_field('contact_form_shortcode');
$newsletter = get_field('newsletter');

if ( $client_logos ): ?>

<div class="section">
    <div class="container">
        <?php get_template_part('template-parts/flexible-content/logo_carousel'); ?>
    </div>
</div>

<?php endif; ?>

<?php if ( $contact_form && $cf_shortcode ):


// Get default values
$accordion = get_field('accordion', 'options');
$form_title = get_field('form_title', 'options');
$form_desc = get_field('form_description', 'options');
$form_sub = get_field('form_subtitle', 'options');

?>

<div class="section section--dark light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-lg-0 mb-5">
                <div class="accordion-container">

                    <?php foreach($accordion as $item): ?>

                    <div class="ac">
                        <h2 class="ac-header">
                            <button class="ac-trigger"><?= $item['title']; ?></button>
                        </h2>
                        <div class="ac-panel">
                            <p class="ac-text">
                                <?= $item['text']; ?>
                            </p>
                        </div>
                    </div>

                    <?php endforeach; ?>

                </div>
            </div>

            <div class="col-lg-8 stack">
                <h3 class="line-title"><?= $form_title; ?></h3>
                <p><?= $form_desc; ?></p>
                <h4 style="color: #FFF;"><?= $form_sub; ?></h4>
                <?php echo do_shortcode($cf_shortcode); ?>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>

<?php if ( $newsletter ): ?>

<?php get_template_part('template-parts/newsletter'); ?>

<?php endif; ?>


<?php
$args = array(
'post_type' => 'post',
'posts_per_page' => 3,
'post_status' => 'publish',
);

$footer_query = new WP_Query($args);

$footer_logo = get_field('footer_logo', 'options');
$footer_description = get_field('footer_description', 'options');
$contact_info = get_field('contact_information', 'options');
$social_links = get_field('social_links', 'options');

// just in case to prevent no timezone set warnings
date_default_timezone_set('Europe/London'); ?>


<footer class="site-footer light" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 mb-lg-0 mb-5">
                <div class="footer-block stack">
                    <?= wp_get_attachment_image($footer_logo['id'], 'full'); ?>
                    <?= $footer_description; ?>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 mb-lg-0 mb-5">
                <div class="footer-block stack">
                    <h4 class="line-title">Latest News</h4>

                    <?php if ( $footer_query->have_posts() ): ?>
                    <?php while ( $footer_query->have_posts() ): $footer_query->the_post(); ?>
                    <div class="footer-post">
                        <?php $thumbnail = get_post_thumbnail_id();

                        if ( $thumbnail ) { ?>
                        <a class="footer-post__img" href="<?php the_permalink(); ?>">
                            <?php echo wp_get_attachment_image( $thumbnail, 'tiny' ); ?>
                        </a>
                        <?php } ?>

                        <div class="footer-post__meta">
                            <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                            <span class="date">Posted <?php the_date('j M Y'); ?></span>
                            <span class="author">By <?php the_author(); ?></span>
                        </div>
                    </div>
                    <?php endwhile; ?>

                    <?php endif; wp_reset_postdata(); ?>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 mb-lg-0 mb-5">
                <div class="footer-block stack">
                    <h4 class="line-title">Contact Information</h4>
                    <?= $contact_info; ?>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 mb-lg-0 mb-5">
                <div class="footer-block stack">
                    <h4 class="line-title">Find us on</h4>
                    <div class="footer-social">
                        <a class="footer-social__icon facebook" href="<?= $social_links['facebook_url']; ?>">
                            <?php include_asset('static/svg/facebook.svg'); ?>
                        </a>
                        <a class="footer-social__icon twitter" href="<?= $social_links['twitter_url']; ?>">
                            <?php include_asset('static/svg/twitter.svg'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer> <!-- /.site-footer -->

<div class="sub-footer">
    <div class="container">
        <div class="sub-footer__items">
            <p>All rights reserved.</p>
            <?php 
                wp_nav_menu( array(
                    'menu'   => 'footer-menu',
                ) ); 
            ?>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>

</html>