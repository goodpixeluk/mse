<?php
	
add_action( 'template_redirect', 'bc_detect_contact_form_submission' );
function bc_detect_contact_form_submission() {
	global $contact_form_errors;
	
	$contact_form_errors = null;

	if ( isset( $_POST['action'] ) && 'submit_contact_form' == $_POST['action'] ) {

		$fields = $_POST;
		// Do any post-processing of field values here

		$contact_form = new ContactForm( $fields );

		$form_validated = $contact_form->validate();
		$contact_form_errors = $contact_form->errors;
		
		if ( $form_validated ) {			
			if ( $contact_form->send_email() ) {
				// Trailing hash removes any previous hash location set
				$redirect_to = home_url('/thank-you/#');

				wp_redirect( $redirect_to );
				exit();
			}
		}

	}
}

if ( !class_exists('ContactForm') ) {
	class ContactForm {
		/**
		 * The values entered into each field in the form.
		 * @var array
		 */
		private $fields;

		/**
		 * An associative array containing any error strings.
 		 * The keys will be the name of the field that caused the error.
 		 * The values will be the error message to be shown to the user.
 		 * The '_form' key is reserved for errors that are global to the form.
		 * @var array
		 */
		public $errors;

		/**
		 * Google reCAPTCHA API key for server side validation.
		 * Generate at https://www.google.com/recaptcha/admin
		 * Leave blank if not using reCAPTCHA on this form.
		 * @var string
		 */
		const RECAPTCHA_SECRET_KEY = '';

		/**
		 * Constructor
		 * @param array $fields An associative array of the values entered into the form.
		 *                      Keys should be the field names.
		 */                      
		function __construct( $fields ) {
			$this->fields = $fields;

			$this->errors = array();
		}

		/**
		 * Add an error to the class property.
		 * @param string $name    The field name.
		 * @param string $message The error message.
		 */
		public function add_error( $name, $message ) {
			$this->errors[ $name ] = $message;
		}

		/**
		 * Validate all fields in the form.
		 * @return boolean True if the form validated, false otherwise.
		 *                      Errors can be retrieved from the $this->errors property.
		 */
		public function validate() {
			// Check that all expected form fields are present to prevent POST request spoofing
			if (
				!isset( $this->fields['fullname'] ) ||
				!isset( $this->fields['email'] ) ||
				!isset( $this->fields['phone'] )
			) {
				$this->add_error( '_form', '<strong>An unknown error occurred and your enquiry could not be sent.</strong><br> Please try again later.' );
				return false;
			}

			
			// Check honeypot fields
			if ( '' !== $this->fields['url'] || isset($this->fields['_accept_terms']) ) {
				// Do not return a recognisable error string
				// Set 404 page status to deter bots further
				http_response_code(404);
				return false;
			}


			if ( $this::RECAPTCHA_SECRET_KEY && !$this->_validate_recaptcha() ) {
				$this->add_error( '_form', '<strong>Your enquiry did not pass our security checks.</strong><br> Please try again later.' );
				return false;
			}


			if ( '' === $this->fields['fullname'] ) {
				$this->add_error( 'fullname', 'Enter your name' );
			}	else if ( strlen( $this->fields['fullname'] ) < 2 ) {
				$this->add_error( 'fullname', 'Enter at least 2 characters' );
			}


			if ( '' === $this->fields['email'] ) {
				$this->add_error( 'email', 'Enter your email address' );
			}	else {
				$this->fields['email'] = filter_var( $this->fields['email'], FILTER_SANITIZE_EMAIL );

				if ( !filter_var( $this->fields['email'], FILTER_VALIDATE_EMAIL ) ) {
					$this->add_error( 'email', 'Enter a valid email address' );
				}
			}	


			if ( '' === $this->fields['phone'] ) {
				$this->add_error( 'phone', 'Enter your phone number' );
			} else {
				$stripped_phone = preg_replace( '/[^0-9+]/', '', $this->fields['phone'] );

				if ( '' == $stripped_phone || strlen( $stripped_phone ) < 9 ) {
					$this->add_error( 'phone', 'Enter a valid phone number' );
				}
			}	

			return count( $this->errors ) === 0;
		}

		private function _validate_recaptcha() {
			$post_url = 'https://www.google.com/recaptcha/api/siteverify';
			$data = array(
				'secret' 	 => $this::RECAPTCHA_SECRET_KEY,
				'response' => $this->fields['g-recaptcha-response'],
				'remoteip' => $_SERVER['REMOTE_ADDR'],
			);	

			$options = array(
				'http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query($data),
				),
			);
			$context = stream_context_create($options);

			$verify = file_get_contents( $post_url, false, $context );
			$captcha_success = json_decode( $verify );

			return $captcha_success->success;
		}

		public function send_email() {
			$_recipient_test = 'Adam Taylor <AdamTaylor@core-marketing.co.uk>, Warren Drumm <WarrenDrumm@core-marketing.co.uk>';

			$_recipient_real = 'Tailored Workspaces <info@netzerosolihull.co.uk>';


			$recipient = $_recipient_real;
			// Check that this is received, and is not blocked by mail server spoofing checks
			$from = 'Tailored Workspaces <noreply@netzerosolihull.co.uk>';
			$subject = 'New Enquiry Online | Tailored Workspaces';
			$headers = array(
			    "From: " . $from,
			    "Reply-To: ". $from,
			    "MIME-Version: 1.0",
			    "Content-Type: text/html; charset=ISO-8859-1"
			  );

			ob_start();

			?>
				<table>
			    <tr>
			      <td colspan="2">A new enquiry was entered online at Tailored Workspaces.</td>
			    </tr>
			    <tr><td colspan="2">&nbsp;</td></tr>
			    <tr>
			      <td><b>Name: </b></td>
			      <td><?php echo $this->fields['fullname'] ?></td>
			    </tr>
			    <tr>
			      <td><b>Email: </b></td>
			      <td><?php echo $this->fields['email'] ?></td>
			    </tr>
			    <tr>
			      <td><b>Phone: </b></td>
			      <td><?php echo $this->fields['phone'] ? $this->fields['phone'] : '[not entered]'; ?></td>
			    </tr>
			  </table>
			<?php

			$body = ob_get_clean();
		   
		  return mail(
		    $recipient,
		    $subject,
		    $body,
		    implode("\r\n", $headers)
		  );
		}
	}
}
