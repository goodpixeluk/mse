<?php

/**
 * Define the custom image sizes used by this theme.
 * @return array The image sizes required.
 */
function bc_get_theme_image_sizes() {
  $image_sizes = array();

  /** Example
  $image_sizes[ 'banner' ] = array(
    'width' => 1600,
    'height' => 500,
    /**
     * Whether to crop the image to the exact dimensions specified.
     * true = hard crop, will lose some of the image.
     * false = soft crop, the whole image will be visible but may not 
     *         be the exact width and height specified above.
     *         The image will be scaled to the largest of the 2 dimensions.
    * /
    'hard_crop' => true,
  );
  //*/

  // Use hard crops for all default WordPress sizes
  $image_sizes[ 'medium' ] = array(
    'width' => get_option( 'medium_size_w' ), 
    'height' => get_option( 'medium_size_h' ),
    'hard_crop' => true,
  );
  $image_sizes[ 'large' ] = array(
    'width' => get_option( 'large_size_w' ), 
    'height' => get_option( 'large_size_h' ),
    'hard_crop' => true,
  );

  $image_sizes[ 'flip_card' ] = array(
    'width' => 545, 
    'height' => 406,
    'hard_crop' => true,
  );

  $image_sizes[ 'gallery' ] = array(
    'width' => 770, 
    'height' => 578,
    'hard_crop' => true,
  );

  $image_sizes[ 'news_thumb' ] = array(
    'width' => 544, 
    'height' => 150,
    'hard_crop' => true,
  );

  $image_sizes[ 'image_box' ] = array(
    'width' => 275, 
    'height' => 79,
    'hard_crop' => true,
  );

  $image_sizes[ 'tiny' ] = array(
    'width' => 70, 
    'height' => 70,
    'hard_crop' => true,
  );


  // $soft_crops = bc_create_crop_set('soft', 320, -1, true);

  // $banner_crops = bc_create_crop_set('banner', 320, 160, true);
  // $banner_narrow_crops = bc_create_crop_set('banner_narrow', 320, 130, true);
  // $banner_deep_crops = bc_create_crop_set('banner_deep', 320, 191, true);

  // $card_crops = bc_create_crop_set('card', 320, 288);

  // $image_sizes = array_merge($image_sizes, $soft_crops, $banner_crops, $banner_narrow_crops, $banner_deep_crops, $card_crops);
  

  /**
   * Filter the image sizes.
   * @param array $image_sizes The custom sizes currently defined.
   * @return array
   */
  return apply_filters( 'bc_custom_image_sizes', $image_sizes );
}

/**
 * Create a set of image sizes which scale from a minimum width and height. 
 * @param  string  $group      A name for the set of images.
 * @param  integer $min_width  The starting width, thumbnail size, for this set of images.
 * @param  integer $min_height The starting height, thumbnail size, for this set of images. 
 *                             Set to -1 to indicate a soft crop where the 
 *                             height is auto defined based on the image width.
 * @param  boolean $include_full_crop True to include a full viewport crop as well.
 * @return array               A 3 element array containing 'thumbnail',
 *                             'medium' & 'large' image sizes.
 */
function bc_create_crop_set( $group, $min_width, $min_height = -1, $include_full_crop = false ) {
  $sizes = array();
  $size_names = array('thumbnail', 'medium', 'large');

  foreach ($size_names as $key => $size_name) {
    $width = $min_width * pow(2, $key);
    $height = $min_height * pow(2, $key);

    // Constrain to max
    $width = min($width, 9999);
    $height = min($height, 9999);

    $sizes[$group . '-' . $size_name] = array(
      'width' => $width,
      'height' => -1 === $min_height ? 9999 : $height,
      'hard_crop' => -1 === $min_height ? false : true,
    );
  }

  if ( $include_full_crop ) {
    $height = floor($min_height / $min_width * 2048);
    $height = min($height, 9999);

    $sizes[$group . '-full'] = array(
      'width' => 2048,
      'height' => -1 === $min_height ? 9999 : $height,
      'hard_crop' => -1 === $min_height ? false : true,
    );
  }

  return $sizes;
}