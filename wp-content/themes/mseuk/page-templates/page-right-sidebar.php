<?php

/**
 * Template Name: Right Sidebar
 */

// keep track of if a section has been opened
global $_section_open;
global $button_styles;

$sidebar = get_field('sidebar');

get_header(); 
get_template_part('template-parts/page-title');

?>

<?php while ( have_posts() ): ?>
<?php
		the_post();
	?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="row">

            <div class="col-lg-9">
                <div id="content" role="main" class="no-container-padding">
                    <?php $_section_open = true; ?>

                    <?php get_template_part('template-parts/flexible-content'); ?>
                </div>
                <?php $_section_open = false; ?>
            </div>

            <aside class="sidebar col-lg-3" role="sidebar">
                <?php dynamic_sidebar($sidebar); ?>
            </aside>

        </div>
    </div>
</article>

<?php endwhile; ?>

<?php get_footer(); ?>