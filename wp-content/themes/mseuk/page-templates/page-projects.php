<?php

/**
 * Template Name: Projects Page
 */


get_header(); 
get_template_part('template-parts/page-title');

?>

<?php while ( have_posts() ): ?>
<?php the_post();

$args = array(
    'post_type'         => 'project',
    'post_status'       => 'publish',
    'posts_per_page'    => -1,
);

$projects = new WP_Query( $args );

?>

<article role="main" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="section pt-10" id="content">
        <div class="container">
            <div class="row">
                <?php if ( $projects->have_posts() ):
                
                while ( $projects->have_posts() ): $projects->the_post(); 
                
                $pt_id = get_post_thumbnail_id();
                
                ?>

                <div class="col-lg-4 col-md-6">
                    <div class="flip-card">
                        <a href="<?php the_permalink(); ?>" class="flip-card__img flip-card__img--bg"
                            style="background-image: url('<?php echo wp_get_attachment_image_url($pt_id, 'flip_card'); ?>')">
                        </a>
                        <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                    </div>
                </div>

                <?php endwhile;

                endif; wp_reset_postdata(); ?>
            </div>
        </div><!-- /. container -->
    </div>

</article>

<?php endwhile; ?>

<?php get_footer(); ?>