<?php

/**
 * Template Name: Full-Width Page
 */

// keep track of if a section has been opened
global $_section_open;
global $button_styles;

get_header(); 
get_template_part('template-parts/page-title');

?>

<?php while ( have_posts() ): ?>
<?php
		the_post();
	?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div id="content">
        <?php $_section_open = true; ?>

        <?php get_template_part('template-parts/flexible-content'); ?>
    </div>
    <?php $_section_open = false; ?>

</article>

<?php endwhile; ?>

<?php get_footer(); ?>