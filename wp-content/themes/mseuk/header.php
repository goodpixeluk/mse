<?php

/**
 * The template for displaying the header.
 */

?>
<!DOCTYPE html>
<html <?php html_class(); ?> <?php language_attributes(); ?>>

<head>
    <title><?php trim(wp_title('')); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&family=Open+Sans&display=swap"
        rel="stylesheet">

    <!-- Start Favicons -->
    <!-- End Favicons -->

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="top">

    <header class="site-header js-site-header" role="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 logo-wrap">
                    <a class="logo" href="<?php echo get_site_url(); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/static/images/mse-logo-white.png"
                            alt="MSE UK - Mechanical & Electrical Contractors.">
                    </a>

                    <button class="hamburger hamburger--squeeze" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>

                <div class="col-md-9 p-relative">
                    <a href="tel:01527 582730" class="num-link"><span class="number">01527 582730</span></a>
                    <?php get_template_part('template-parts/site-nav'); ?>
                </div>
            </div>
        </div>
    </header> <!-- /.site-header -->