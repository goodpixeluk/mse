<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 */
// keep track of if a section has been opened
global $_section_open;

get_header(); 

?>

<?php while ( have_posts() ): ?>
	<?php
		the_post();
	?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

		<?php // get_template_part('template-parts/hero'); ?>

		<div class="section" id="content">
			<?php $_section_open = true; ?>

			<?php get_template_part('template-parts/flexible-content'); ?>
		</div>
		<?php $_section_open = false; ?>

	</article>

<?php endwhile; ?>

<?php get_footer(); ?>
