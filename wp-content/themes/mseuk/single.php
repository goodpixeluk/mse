<?php

/**
 * The Template for displaying all single posts
 */

global $post;

get_header(); 
get_template_part('template-parts/page-title');

$image_id = get_post_thumbnail_id();
if ( $image_id ) {
	$caption = wp_get_attachment_caption($image_id);
}

?>

<?php while ( have_posts() ): ?>
<?php
		the_post();
	?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="section pt-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h1 class="post-title mb-4"><?= get_the_title(); ?></h1>
                    <div class="news-page-preview__meta">
                        <span class="date">Posted on <span><?php the_date('j M Y'); ?></span></span>
                        <span class="author">By <span><?php the_author(); ?></span></span>
                    </div>

                    <div class="single-post__content mt-4">
                        <?php the_content(); ?>
                    </div>
                </div>
                <aside class="pt-0 sidebar col-lg-3" role="sidebar">
                    <?php dynamic_sidebar('news'); ?>
                </aside>
            </div>


        </div>
    </div>

</article>

<?php endwhile; ?>

<?php get_footer(); ?>