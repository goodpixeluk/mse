<?php

/**
 * Template Name: Key Theme Page
 */

// keep track of if a section has been opened
global $_section_open;

get_header(); ?>

<?php while ( have_posts() ): the_post(); ?>

<?php get_template_part('template-parts/hero'); ?>

<div id="content" role="main">
    <?php $_section_open = true; ?>

    <?php get_template_part('template-parts/flexible-content'); ?>
</div>
<?php $_section_open = false; ?>

<?php endwhile; ?>

<?php get_footer(); ?>