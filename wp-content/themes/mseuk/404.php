<?php

/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); 

?>

<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-16 offset-lg-4 col-md-20 offset-md-2 pt-8 pb-16">
                <h1 data-aos="fade-up" class="h2 pink">Sorry, that <br>doesn't exist.</h1>

                <div class="flow flow--basic text-container" data-aos="fade-up" data-aos-delay>
                    <p>&nbsp;</p>
                    <p>We couldn't find the page you were looking for.</p>
                    <p>If you typed the URL, check for spelling errors. If you followed a link, try going back in your
                        browser or refreshing the page.</p>
                    <p>&nbsp;</p>
                    <p>
                        <a class="button" href="<?= home_url(); ?>">
                            Back to Home
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>