<?php

/**
 * The acrhive page
 */

// keep track of if a section has been opened
global $_section_open;
global $button_styles;

get_header(); 
get_template_part('template-parts/page-title');


?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="row">

            <aside class="sidebar col-lg-3" role="sidebar">
                <?php dynamic_sidebar('news'); ?>
            </aside>

            <div class="col-lg-9">
                <div id="content" role="main" class="section no-container-padding">


                    <?php while ( have_posts() ): the_post(); ?>
                    <div class="news-page-preview stack">
                        <?php $thumbnail = get_post_thumbnail_id();

                        if ( $thumbnail ) { ?>
                        <a href="<?php the_permalink(); ?>">
                            <?php echo wp_get_attachment_image( $thumbnail, 'full' ); ?>
                        </a>
                        <?php } ?>

                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                        <div class="npp-flex">
                            <div class="news-page-preview__meta">
                                <span class="date">Posted on <span><?php the_date('j M Y'); ?></span></span>
                                <span class="author">By <span><?php the_author(); ?></span></span>
                            </div>
                            <a class="button" href="<?php the_permalink(); ?>">Read more</a>
                        </div>
                    </div>
                    <?php endwhile; ?>

                </div>
            </div>

        </div>
    </div>
</article>


<?php get_footer(); ?>