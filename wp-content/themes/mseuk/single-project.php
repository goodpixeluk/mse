<?php

/**
 * The Template for displaying all project posts
 */

global $post;

get_header(); 
get_template_part('template-parts/page-title');

$image_id = get_post_thumbnail_id();
if ( $image_id ) {
	$caption = wp_get_attachment_caption($image_id);
}

$post_id = get_the_ID();

$args = array(
    'post_type'         => 'project',
    'post_status'       => 'publish',
    'post__not_in'      => array($post_id),
    'posts_per_page'    => 4,
);

$projects = new WP_Query( $args );

$project_value = get_field('project_value');
$project_gallery = get_field('project_gallery');

?>

<?php while ( have_posts() ): ?>
<?php
		the_post();
	?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="section pt-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="slider slider--project">
                        <div class="js-slider">
                            <?php foreach( $project_gallery as $item ) { ?>
                            <div class="project-gallery__slide">
                                <?php echo wp_get_attachment_image($item['id'], 'gallery'); ?>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="slider__buttons">
                            <button class="prev"><span class="sr-only">Previous Slide</span></button>
                            <button class="next"><span class="sr-only">Next Slide</span></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 pt-5 pt-md-0">
                    <div class="stack">
                        <h3 class="line-title">Project Description</h3>

                        <?php if ( get_field('project_logo') ) {
                            echo wp_get_attachment_image( get_field('project_logo')['id'], 'full' );
                        } ?>

                        <?php the_field('project_description'); ?>

                        <?php if ( $project_value ) { ?>
                        <p><strong>Project Value:</strong> <?= $project_value; ?></p>
                        <?php } ?>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="section">
        <div class="container stack">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="line-title">
                        Related Work
                    </h3>
                </div>
            </div>
            <div class="row">
                <?php if ( $projects->have_posts() ):
                    
                    while ( $projects->have_posts() ): $projects->the_post(); 
                    
                    $pt_id = get_post_thumbnail_id();
                    
                    ?>

                <div class="col-sm-6 col-md-3">
                    <div class="flip-card">
                        <a href="<?php the_permalink(); ?>" class="flip-card__img flip-card__img--bg"
                            style="background-image: url('<?php echo wp_get_attachment_image_url($pt_id, 'flip_card'); ?>')">
                        </a>
                        <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                    </div>
                </div>

                <?php endwhile;

                    endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>

</article>

<?php endwhile; ?>

<?php get_footer(); ?>